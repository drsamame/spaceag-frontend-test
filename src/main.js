import Vue from 'vue';
import { Icon } from 'leaflet';
import App from './App.vue';
import router from './router';
import store from './store';
import './registerServiceWorker';
import $backend from '@/backend';
import 'leaflet/dist/leaflet.css';
import numericOnly from './directives/numericOnly';

Vue.prototype.$backend = $backend;
Vue.config.productionTip = false;
Vue.directive('numeric-only', numericOnly);

// eslint-disable-next-line no-underscore-dangle
delete Icon.Default.prototype._getIconUrl;

Icon.Default.mergeOptions({
  // eslint-disable-next-line global-require
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  // eslint-disable-next-line global-require
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  // eslint-disable-next-line global-require
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
