import axios from 'axios';

const $backend = axios.create({
  baseURL: '/',
  timeout: 5000,
  headers: { 'Content-Type': 'application/json' },
});

$backend.$fetchGeo = () => $backend.get('https://analytics.spaceag.co/static/challenges_info/one_polygon_carto.json')
  .then(response => response.data);

export default $backend;
